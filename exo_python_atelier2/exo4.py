from exo2 import est_bissextile
from datetime import date

def date_est_valide(jour: int,mois: int,annee: int):

    if est_bissextile(annee):
        if jour == 29 and mois == 2:
            result = False
        else:
            result = True
    else:
        result = True
    return result

def saisie_date_naissance():
    anne, mois, jour = "", "", ""
    print("Qelle est votre annee de naissance ?")
    while anne == "":
        anne = input()
        if len(anne) == 4 and anne.isnumeric():
            if date(int(anne),1,1) > date.today():
                anne = ""
                print("C'est impossible veuillez entrer une année inferieur a l'année actuelle")
        else:
            anne = ""
            print("Veuillez renseigner une annee de naissance valide")

    print("quelle est votre mois de naissance ?")
    while mois == "":
        mois = input()
        if mois.isnumeric():
            if int(mois) <= 0 or int(mois) > 12:
                mois = ""
                print("Veuillez renseigner un mois de naissance compris entre 1 et 12")
        else:
            mois = ""
            print("Veuillez renseigner un nombre entier positif")

    print("quelle est votre jour de naissance ?")
    while jour == "":
        jour = input()
        if jour.isnumeric():
            if (int(jour) <= 0 or int(jour) > 31):
                jour = ""
                print("Veuillez renseigner un jour de naissance compris entre 1 et 31")
        else:
            jour = ""
            print("Veuillez renseigner un nombre entier positif")

    anne, mois, jour = int(anne),int(mois),int(jour)
    if date_est_valide(jour, mois,anne ):
        return date(anne, mois, jour)

def age(date_naissance):
    age = (date.today() - date_naissance) / 365
    return age.days

def est_majeur(date_naissance):
    AGE = age(date_naissance)
    if AGE > 18:
        result = True
    else:
        result = False
    return result

def test_acces ():
    DATE_NAISS = saisie_date_naissance()
    if DATE_NAISS:
        AGE = age(DATE_NAISS)
        EST_MAJ =  est_majeur(DATE_NAISS)
        if EST_MAJ:
            print("Bonjour, vous avez " +  str(AGE) + " ans, Accès autorisé ")
        else:
            print("Bonjour, vous avez " +  str(AGE) + " ans, Accès interdit ")
test_acces()
