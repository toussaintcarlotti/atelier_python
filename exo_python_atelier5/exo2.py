from exo1 import gen_list_random_int

def mix_list(list_to_mix: list) -> list:
    """
    fonction qui prend une liste potentiellement triée en parametre et renvoi la liste correspondante mélanger
    @param list_to_mix: list
    @return: list
    """
    lst_rand = []
    #création d'une liste d'indice mélanger correspondant a la liste entrer en paramètre

    lst_irand = gen_list_random_int(0, len(list_to_mix))
    # parcour de la liste d'indice mélangée afin de créer une liste d'elements mélanger

    for e in lst_irand:
        # ici l'element e correspond donc a un indice de la liste list_to_mix
        lst_rand.append(list_to_mix[e])
    return lst_rand

def test():
    """
    fonction de test
    """
    lst_sorted = [i for i in range(10)]
    print(lst_sorted)
    print('Liste triée de départ', lst_sorted)
    mixed_list = mix_list(lst_sorted)
    print('Liste mélangée obtenue', mixed_list)
    print('Liste triée de départ après appel à mixList, elle doit être inchangée',
          lst_sorted)
    # assert (cf. doc en ligne) permet de vérifier qu’une condition
    # est vérifiée en mode debug (désactivable)
    assert lst_sorted != mixed_list, "Les deux listes doivent être différente " \
                                     "après l'appel à mixList..."

