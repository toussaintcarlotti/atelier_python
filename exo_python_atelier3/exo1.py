def somme1(L: list) -> float:
    somme = 0
    for i in range(len(L)):
        somme = somme + L[i]
    return somme

def somme2(L: list) -> float:
    somme = 0
    for e in L:
        somme = somme + e
    return somme

def somme3(L: list) -> float:
    i, somme = 0, 0
    while i < len(L) :
        somme = somme + L[i]
        i += 1
    return somme

def test_exo1():
    print("TEST SOMME")
    # test liste vide
    print("Test liste vide : \nversion1: {}\nversion2: {}"
          "\nversion3: {}".format(somme1([]),somme2([]),somme3([])))
    # test somme 11111
    S = [1, 10, 100, 1000, 10000]
    print("Test somme 11111 : \nversion1: {}\nversion2: {} "
          "\nversion3: {}".format(somme1(S),somme2(S),somme3(S)))


def moyenne(L: list) -> float:
    result = 0
    if len(L) > 0:
        result = somme2(L) / len(L)
    return result

liste = []
print( "la moyenne de la liste {} est {}".format(liste,moyenne(liste)))


def nb_sup1(L: list,e: float) -> int:
    result = 0
    for i in range(len(L)):
        if L[i] > e:
            result = result + 1
    return result

def nb_sup2(L: list,e:float) -> int:
    result = 0
    for parcour in L:
        if parcour > e:
            result = result + 1
    return result

print(str(nb_sup1([1,2,3,4,5],2)) +"\n"+ str(nb_sup2([1,2,3,4,5],2)))


def moy_sup(L: list, e:float) -> float:
    L2 = []
    for parcour in L:
        if parcour > e:
            L2.append(parcour)
    return moyenne(L2)

print(str(moy_sup([1,2,3,4,5,6,7,8,9,10],4)))

def val_max(L: list) -> float:
    result = L[0]
    for i in range(len(L) - 1):
        if result < L[i+1]:
            result = L[i+1]
    return result

print(val_max([1,8,2,7,2,6,84,10,5,200,7,49]))


def ind_max(L: list) -> float:
    test, result = L[0], 0
    for i in range(len(L) - 1):
        if test < L[i+1]:
            test = L[i+1]
            result = i + 1
    return result

print(val_max([1,8,2,7,2,6,84,10,5,200,7,49]))
print(ind_max([1,8,2,7,2,6,84,10,5,200,7,49]))


