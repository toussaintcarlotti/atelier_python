from exo2 import mix_list

def extract_elements_list(list_in_which_to_choose: list,
                          int_nbr_of_element_to_extract: int) -> list:
    """
    fonction qui prend en paramètre une liste et retourne
    une liste composée de n éléments de
    cette liste  choisis au hasard

    @param list_in_which_to_choose: list
    @param int_nbr_of_element_to_extract: int
    @return: list
    """

    return mix_list(list_in_which_to_choose)[:int_nbr_of_element_to_extract]

#print(extract_elements_list([1,2,3,4,5,6,7,8,9],5))

def test():
    lst_sorted = [i for i in range(10)]
    print('Liste de départ', lst_sorted)
    sublist = extract_elements_list(lst_sorted, 5)
    print('La sous liste extraite est', sublist)
    print('Liste de départ après appel de la fonction est', lst_sorted)
