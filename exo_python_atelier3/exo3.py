from color import bcolors

def separer(lst: list) -> list:
    """
    Permet de séparer une liste (-x, 0, x)
    :param lst: list
    :return: list
    """

    lst_neg, lst_eg, lst_pos = [], [], []

    #parcours de la liste lst afin de ranger chaque element dans la liste correspondante
    for e in lst:
        if e < 0:
            lst_neg.append(e)
        elif e == 0:
            lst_eg.append(e)
        else:
            lst_pos.append(e)
    return lst_neg + lst_eg + lst_pos


def test_separer() -> bool:
    print("RAPPEL : Les resultat attendu est une liste separer, les negatifs en premier, les null ensuite et les positif a la fin.\nPar exemple [-1, 0, 1]")
    lst_lst = [-1,5,4,-2,0,7,0,-7,4,3],[1,2,3,4,5,6,7,8],[-4,-8,-7,0,0,0,4,5,8],[0,-1],[-1,1]
    lst_lst_att = [-1, -2, -7, 0, 0, 5, 4, 7, 4, 3],[1, 2, 3, 4, 5, 6, 7, 8],[-4, -8, -7, 0, 0, 0, 4, 5, 8],[-1, 0],[-1,1]
    pos_0 = 3,
    result_t = False
    for i in range(len(lst_lst)):
        if lst_lst[i] == lst_lst_att[i]:
                result_t = True
        print("Sépararion de la liste :" + str(lst_lst[i]) + bcolors.OKBLUE + "\nResultat: " + str(separer(lst_lst[i])) + bcolors.OKGREEN +
              " | Attendu: "+ str(lst_lst_att[i]) + bcolors.ENDC)
    return result_t
test_separer()


