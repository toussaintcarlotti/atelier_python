from math import *

def discriminant(a: float, b: float, c: float):
    """ Fonction qui trouve le discriminant delta """
    #formule donner
    delta = b*b-4*a*c
    return delta

def racine_unique(a: float, b: float):
    """ Fonction qui renvoie la valeur de la racine """
     #formule donner
    x = -b / 2 * a
    return x

def racine_double(a: float, b: float,delta: float ,num: int):
    """ Fonction qui renvoie la valeur des deux racines """
    #calcul de la solution demandée(x1 ou x2)
    if num == 1:
         #formule correspondante
        x = (-b + sqrt(delta)) / (2 * a)
    elif num == 2:
        x = (-b - sqrt(delta)) / (2 * a)
    return x

def str_equation(a: float, b: float, c: float):
    """ Fonction qui renvoi une chaine de caractere qui represente l'equation du second degré """
    return "{}x² + {}x + {} = 0".format(a,b,c)

def solution_equation(a: float, b: float, c: float):
    delta = discriminant(a,b,c)
    if delta < 0:
        message = "Solution de l'équation " + str_equation(a,b,c) \
                  + "\nPas de racine réelle"
    elif delta == 0:
        message = "Solution de l'équation " + str_equation(a,b,c) \
                  + "\nRacine unique : x= " + str(racine_unique(a,b))
    else:
        message = "Solution de l'équation " + str_equation(a,b,c) + \
              "\nRacine double :\nx1=  " + str(racine_double(a,b,delta,1)) + \
              "\nx2= " + str(racine_double(a,b,delta,2))
    return message

def equation(a: int, b: int, c: int):
    delta = discriminant(a,b,c)
    if delta < 0:
        print("aucune solution réelle")
    elif delta == 0:
        print("x= " + str(racine_unique(a,b)))
    else:
        print("x1= " + str(racine_double(a,b,delta,1)) + \
              "\nx2= " + str(racine_double(a,b,delta,2)))


def test():
    print(str_equation(1,2,-8))
    print(discriminant(1,2,-8))
    print(solution_equation(1,2,-8))
    print(equation(1,2,-8))



test()
