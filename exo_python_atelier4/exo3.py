import random
from color import bcolors

def places_lettre(ch : str, mot : str) -> list:
    """
    fonction recherche si le caractère char est présent dans
    la chaine mot et renvoie une liste vide si le caractère
    n'est pas présent et sinon le ou les indices désignants
    sa place dans le mot

    @param ch: str
    @param mot: str
    @return: list
    """
    lst = []
    for i, e in enumerate(mot):
        if e == ch:
            lst.append(i)
    return lst


def outputStr(mot: str, lpos: list) -> str:
    """
    renvoie une chaîne de caractères comprenant tous les caractères
    de la chaine mot ou certains caractères
    remplacés par des tirets

    @param mot: str
    @param lpos: list
    @return: str
    """
    mot_r = "_" * len(mot)
    for e in lpos:
        for i, e1 in enumerate(mot):
            if i == e:
                mot_r = mot_r[:i] + e1 + mot_r[i + 1:]
    return mot_r

def build(fichier):
    file, content = open(fichier), []
    content.append(file.readline())
    while content[len(content)-1] != '':
        content.append(file.readline().replace('\n', '').lower())
    file.close()
    del content[len(content) - 1]
    return content

def runGame(fichier: str):
    lst_mot = build(fichier)
    iRand = random.randint(1, len(lst_mot))
    MOT = lst_mot[iRand]
    mot_r, compt, lst_i = outputStr(MOT, []), 0, []
    CHANCE = 5
    lst_car_dess, dessin = ["\n|______"," \\", "\n|   / ", "\n|    T","\n|    O"], ""

    while len(lst_i) < len(mot_r) and compt < CHANCE:
        lettre = input("Veuillez saisir une lettre\n")
        #lst_i += places_lettre(lettre, MOT)
        lst_i.extend(places_lettre(lettre, MOT)) #ajout a lst_i les indices correspondant au caractere demander a l'utilisateur
        print(outputStr(MOT, lst_i))
        if not places_lettre(lettre, MOT):
            dessin = lst_car_dess[0] + dessin
            lst_car_dess = lst_car_dess[1:]
            compt += 1
        print(dessin)

    if compt == CHANCE:
        print(f"{bcolors.FAIL} PERDU !!!! {bcolors.ENDC}")
    else:
        print(f"{bcolors.OKGREEN} Gagné !!! {bcolors.ENDC}")

runGame("littre.txt")
build("littre.txt")

def test():
    places_lettre('a','bonjour')
    places_lettre('m','maman')
    print(outputStr("testerd",[1,2,4]))

