def message_imc(imc: float):
    """ Fonction qui indique l'état de santé d'une personne par rapport a son imc """

    # Attribution message en fonction de l'imc
    if imc > 40:
        result = "obésité morbide"
    elif imc > 35:
        result = "obésité sévère"
    elif imc > 30:
        result = "obésité modérée"
    elif imc > 25:
        result = "surpoids"
    elif imc > 18.5:
        result = "corpulence normale"
    elif imc > 16.5:
        result = "maigreur"
    else:
        result = "dénutrition ou famine"
    return result

def test():
    """ Fonction qui test a plusieurs reprise la fonction message_imc """
    print( message_imc(12) + ", " + message_imc(17) + ", " +  message_imc(32)
           + ", " + message_imc(20) + ", " + message_imc(50) + ", " + message_imc(38))

test()
