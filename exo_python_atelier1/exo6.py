def fraisMensuel():
    typeCarburant = ""
    km = int(input("Entrer le nombre de kilometre parcouru:"))
    print("Entrer le type de carburant du vehicule (essence ou diesel):")
    CONSO_8l_100 = 100 / 8
    CONSO_10l_100 = 100 / 10
    # verification
    while typeCarburant == "":
        a = input()
        if a == "essence" or a == "E" or a == "Essence":
            typeCarburant = "E"
        elif a == "diesel" or a == "D" or a == "Diesel":
            typeCarburant = "D"
        else:
            print("Veuillez entrer un type de carburant valide (essence ou diesel)")
    print("Quelle est la cylindrée du véhicule ?")
    cylVehicule = int(input())
    print("Renseigner le prix actuel pour 1 litre de carburant")
    prixCarb = float(input())

    if typeCarburant == "E":
        if cylVehicule > 2000:
            fraisM = (km/CONSO_10l_100) * prixCarb
        else:
            fraisM = (km/CONSO_8l_100) * prixCarb
        fraisM = fraisM * 1.5

    if typeCarburant == "D":
        fraisM = (0.08 * km) * prixCarb
        fraisM = fraisM * 1.7

    fraisM = fraisM / 12
    print(fraisM)
fraisMensuel()
