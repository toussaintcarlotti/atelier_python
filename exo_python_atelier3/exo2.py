def position1(lst: list,e: int) -> int:
    result = -1
    for i,element in enumerate(lst):
        if e == element:
            result = i
    return result

def position2(lst: list,e: int) -> int:
    result, i = -1, 0
    while i < len(lst) and result == -1:
        if e == lst[i]:
            result = i
        i += 1
    return result

print(position1([1,2,3,4,5],2))
print(position2([1,2,3,4,5],2))


def nb_occurrences(lst: list,e: int) -> int:
    result = 0
    for parcour in lst:
        if e == parcour:
            result += 1
    return result

print(nb_occurrences([1,2,3,4,5,2,2,4,2],2))

def est_triee_crsst(lst: list) -> float:
    result = True
    for i in range(len(lst) - 1):
        if lst[i] > lst[i+1]:
            result = False
    return result
print(est_triee_crsst([1,2,8,4,5]))


def est_triee_double(lst: list) -> float:
    result, i = True, 0
    while result == True and i < len(lst) - 1:
        if lst[i] > lst[i+1]:
            result = False
        i += 1
    if result == False:
        result, i = True, 0
        while result == True and i < len(lst) - 1:
            if lst[i] < lst[i + 1]:
                result = False
            i += 1
    return result
print(est_triee_double([1,2,3,4,5]))
print(est_triee_double([2,3,4,5,6,7]))
print(est_triee_double([2,3,1,5,6,7]))


def position_tri(lst,e):
    if est_triee_double(lst):
        bmax = len(lst)
        bmin = 0
        mil = round(len(lst) / 2)
        result = -1
        if len(lst) > 0 and nb_occurrences(lst,e):
            if est_triee_crsst(lst):
                while lst[mil] != e :
                    if e > lst[mil]:
                        bmin = mil
                        mil = round(mil + ((bmax - mil) / 2))
                    elif e < lst[mil]:
                        bmax = mil
                        mil = round((mil - bmin) / 2)
                result = mil
            else:
                while lst[mil] != e :
                    if e < lst[mil]:
                        bmin = mil
                        mil = round(mil + ((bmax - mil) / 2))
                    elif e > lst[mil]:
                        bmax = mil
                        mil = round((mil - bmin) / 2)
                result = mil
        return result

print(position_tri([1,2,4,8,10,24,30,32,40],0))
print(position_tri([1,2,4,8,10,24,30,32,40],10))
print(position_tri([20,19,18,15,13,11,10,8,5,2,1],11))
print(position_tri([20,19,18,15,13,11,10,8,5,2,1],1))
print(position_tri([20,19,18,15,13,11,10,8,5,2,1],22))

def a_repetitions(lst):
    lst_t, i, result = [], 0, False
    while i < len(lst) and result == False:
        if not nb_occurrences(lst_t, lst[i]):
            lst_t.append(lst[i])
        else:
            result = True
        i += 1

    return result


print(a_repetitions([1]))
print(a_repetitions([1,2,3,5,4]))
print(a_repetitions([1,2,3,5,4,1]))
