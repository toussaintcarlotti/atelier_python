import matplotlib.pyplot

from exo2 import nb_occurrences
from exo1 import val_max
import matplotlib.pyplot as plt

def histo(lst_f: list) -> list:
    """
    Fonction qui retourne l'histogramme d'une liste passée en parametre
    :param lst_f: list
    :return: list
    """
    MAXTAILLE, lst_h = val_max(lst_f) + 1, []
    for i in range(round(MAXTAILLE)):
        lst_h.append(nb_occurrences(lst_f, i))
    return lst_h


print(histo([5,8,4,0,2,4,8,5,7,4,9,8,7,9]))


def est_injective(lst_f: list) -> bool:
    histo_lst_f = histo(lst_f)
    result, i = True, 0
    while i < len(histo_lst_f) and result:
        if histo_lst_f[i] > 1:
            result = False
        i += 1
    return result

print(est_injective([5,8,4,0,2,4,8,5,7,4,9,8,7,9]))
print(est_injective([0,2,4,8,5,7,9]))

def est_surjective(lst_f: list) -> bool:
    histo_lst_f = histo(lst_f)
    result, i = True, 0
    while i < len(histo_lst_f) and result:
        if histo_lst_f[i] <= 1 :
            result = False
        i += 1
    return result

print(est_surjective([0,2,4,8,5,7,9]))
print(est_surjective([0,0,4,0,2,2,4,3,3,1,1]))

def est_bijective(lst_f: list) -> bool:
    histo_lst_f = histo(lst_f)
    result, i = True, 0
    while i < len(histo_lst_f) and result:
        if histo_lst_f[i] != 1:
            result = False
        i += 1
    return result

print(est_bijective([0,0,4,0,2,2,4,3,3,1,1]))
print(est_bijective([0,2,4,8,5,7,9]))
print(est_bijective([0,1,2,3,5,4,6,7]))



def affiche_histo(lst_f: list):
    HISTO_LST = histo(lst_f)
    print(HISTO_LST)
    maxocc = round(val_max(HISTO_LST))
    print("TEST HISTOGRAMME\n F= "+ str(lst_f) +"\n\nHISTOGRAMME\n")
    for i in range(maxocc):
        for e in HISTO_LST:
            if e >= maxocc - i:

                print("  # ",end="")
            else:
                print("    ",end="")
        print("\n")

    for j in range(len(HISTO_LST)):
        print("| --",end="")
    print("|\n")
    for k in range(len(HISTO_LST)):
        print("  " + str(k) + " ",end="")


affiche_histo([0,0,4,0,2,2,3,3,1,1])


def affiche_histo2(lst_f):
    plt.hist(lst_f)
    matplotlib.pyplot.show()

affiche_histo2([0,0,4,5,6,0,2,2,3,3,1,1])
