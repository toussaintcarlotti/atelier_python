def est_bissextile(anne: int):
    """ fonction qui indique si l'année passée en parametre est bissextile """
    verif = False
    if (anne % 4 == 0 and anne % 100 != 0) or (anne % 400 == 0):
            verif = True
    return verif

def test():
    """ Fonction qui test si une année est bissextile """
    print("2020: " + str(est_bissextile(2020)) + ", 2004: " + str(est_bissextile(2004)) + ", 1824: " + str(est_bissextile(1824))
           + ", 2015: " + str(est_bissextile(2015)) )

test()
