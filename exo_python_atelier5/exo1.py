import random

def gen_list_random_int(int_binf = 0, int_bsup = 10) -> list:
    """
    fonction qui génère et retourne une liste de nombres
    aléatoires compris entre deux bornes

    @param int_binf: int
    @param int_bsup: int
    @return: list
    """
    int_nbr = []
    for i in range(int_binf, int_bsup):
        #ajout d'un nombre aléatoire compris entre int_binf et int_bsup
        int_nbr.append(random.randint(int_binf, int_bsup - 1))

        while int_nbr[i] in int_nbr[:i]:#on verifie qu'il ne puisse pas y avoir de double
            int_nbr[i] = random.randint(int_binf, int_bsup - 1)
    return int_nbr

def test():
    """
    fonction de test
    """
    print(gen_list_random_int())

