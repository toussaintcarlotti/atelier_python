import random
from exo2 import mix_list
from exo1 import gen_list_random_int
import time
import matplotlib.pyplot as plt
import numpy as np


def perf_mix(mix_list: callable, shuffle: callable, lst_taillelst: list,
             nb_exec: int) -> (list, list):
    """
    fonction perf_mix qui permet de calculer le temps d’exécution moyen des deux fonctions
    de mélange

    @param mix_list: callable
    @param shuffle: callable
    @param lst_taillelst: list
    @param nb_exec: int
    @return: (list, list)
    """

    lst_lst, lst_time_mix, lst_time_shu = [], [], []
    total_time_mix, total_time_shu = 0, 0
    for e in lst_taillelst:
        lst_lst.append(gen_list_random_int(0, e))

    for e_lst in lst_lst:
        for i in range(nb_exec):

            start_pc_mix = time.perf_counter()
            mix_list(e_lst)
            end_pc_mix = time.perf_counter()
            total_time_mix = total_time_mix + (end_pc_mix - start_pc_mix)

            start_pc_shu = time.perf_counter()
            shuffle(e_lst)
            end_pc_shu = time.perf_counter()
            total_time_shu = total_time_shu + (end_pc_shu - start_pc_shu)
        lst_time_mix.append(total_time_mix / nb_exec)
        lst_time_shu.append(total_time_shu / nb_exec)
    return lst_time_mix, lst_time_shu

print(perf_mix(mix_list, random.shuffle, [5, 10, 15, 20, 100, 300], 100))

'''
def courbe(tuple_list: (list,list)):
    # Ici on décrit les abscisses
    # Entre 0 et 5 en 10 points
    x_axis_list = np.arange(0, len(tuple_list[0]))
    fig, ax = plt.subplots()
    # Dessin des courbes, le premier paramètre
    # correspond aux point d'abscisse le
    # deuxième correspond aux points d'ordonnées
    # le troisième paramètre, optionnel permet de
    # choisir éventuellement la couleur et le marqueur
    ax.plot(x_axis_list, tuple_list[0], 'bo-', label='fonction1')
    ax.plot(x_axis_list, tuple_list[1], 'r*-', label='fonction2')

    ax.set(xlabel='Abscisse x', ylabel='Ordonnée y',
           title='Fonctions identité, cube et carré')
    ax.legend(loc='upper center', shadow=True, fontsize='x-large')
    # fig.savefig("test.png")
    plt.show()

courbe(perf_mix(mix_list, random.shuffle, [5, 10, 15, 20, 100, 300], 100))
'''