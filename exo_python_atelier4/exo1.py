def full_name(str_arg: str) -> str:

    """
    Fonction qui formate le nom et le prenom (NOM Prenom)
    :param str_arg: str
    :return str
    """

    #separation de la liste
    lst_str = str_arg.split()

    return lst_str[0].upper() + " " + lst_str[1].capitalize()




def is_mail(str_arg:str)->(int,int):
    result = (1,0)
    if "@" in str_arg:
        lst_str = str_arg.split("@")
        if len(lst_str[0]) > 1:
            del lst_str[0]
            if "." in lst_str[0]:
                lst_str = lst_str[0].split('.')
                if lst_str[0] != "univ-corse":
                    result = (0,3)
            else:
                result = (0,4)
        else:
            result = (0,1)
    else:
        result = (0,2)
    return result

def test():
    print(full_name("carlotti toussaint"))
    print(is_mail("toussaint.carlotti@univ-corse.fr"))

test()
