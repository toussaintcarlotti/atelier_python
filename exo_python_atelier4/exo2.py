def mots_Nlettres(lst_mot: list, n: int) -> list:
    """
    fonction qui prend une liste de mots (lst_mot) en
    argument et qui renvoie la liste des mots contenant exactement n lettres

    @param lst_mot: list
    @param n: int
    @return: list
    """
    lst_r = []
    for mot in lst_mot:
        if len(mot) == n:
            lst_r.append(mot)
    return lst_r


def commence_par(mot: str, prefixe: str) -> bool:
    """
    renvoie True si l'argument mot
    commence par prefixe et False sinon

    @param mot: str
    @param prefixe: str
    @return: bool
    """
    return mot[:len(prefixe)] == prefixe


def finit_par(mot, suffixe) -> bool:
    """
    renvoie True si l'argument mot se termine
    par suffixe et False sinon

    @param mot: str
    @param suffixe: str
    @return: bool
    """
    return mot[len(mot) - len(suffixe): len(mot)] == suffixe


def finissent_par(lst_mot: list, suffixe: str) -> list:
    """
    renvoie la liste des mots présents
    dans la liste lst_mot qui se terminent par suffixe

    @param lst_mot:
    @param suffixe:
    @return: list
    """
    lst_r = []
    for e in lst_mot:
        if finit_par(e, suffixe):
            lst_r.append(e)
    return lst_r


def commencent_par(lst_mot: list, prefixe: str) -> list:
    """
    renvoie la liste des mots
    présents dans la liste lst_mot qui commencent par prefixe

    @param lst_mot: list
    @param prefixe: str
    @return: list
    """
    lst_r = []
    for e in lst_mot:
        if commence_par(e, prefixe):
            lst_r.append(e)
    return lst_r


def liste_mots(lst_mot: list, prefixe: str, suffixe: str, n: int) -> list:
    """
    renvoie la liste des mots présents dans
    lst_mot qui commencent par prefixe, se terminent par
    suffixe et contiennentexactement n lettres

    @param lst_mot: list
    @param prefixe: str
    @param suffixe: str
    @param n: int
    @return: list
    """
    lst_r = commencent_par(lst_mot, prefixe)
    lst_r = finissent_par(lst_r, suffixe)
    lst_r = mots_Nlettres(lst_r, n)
    return lst_r


def dictionnaire(fichier):
    l_result = []
    f = open(fichier, "r")  # ouverture du fichier en lecture (r=read)
    c = f.readline()  # lecture d'une ligne dans une chaine de caracteres
    print("** Contenu du fichier")
    while c != "":
        # print(c)
        c = f.readline().replace('\n', '')
        l_result.append(c)
    return liste_mots(l_result, "te", "e", 6)
    # print("** fin **")


def test() -> bool:
    result = True
    print(mots_Nlettres(['test', 'gtdvbgg', 'utop', 'rfrsfe', 'rrrr'], 4))

    # print(f"TEST COMMENCE_PAR:\n-demonter ")
    print(commence_par("demonter", "de"))
    print(commence_par("demonter", "se"))

    print(finit_par("demonter", "ter"))
    print(finit_par("demonter", "xer"))

    print(finissent_par(["demonter", "bfhvbxer", "erfever", "vrrxer"], "xer"))
    print(commencent_par("demonter", "se"))

    print(dictionnaire("littre.txt"))
    # print(liste_mots(["test", "tedbchbdst","dcbdsh",'test'],'te','st',4))
    return result


test()
