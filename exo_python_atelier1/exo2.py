# fonction qui lit un caractère puis affiche de quoi il s'agit (lettre minuscule, majuscule, chiffre, caractere special)
def findchar(char):
    """ test """
    if type(char) == int:
        char = str(char)
    char = ord(char)
    # chaque if est defini par rapport au tableau du code ASCII
    if char > 122:
        print("Caractère spécial")
    elif char > 96:
        print("Lettre minuscule")
    elif char > 90:
        print("Caractère spécial")
    elif char > 64:
        print("Lettre majuscule")
    elif char > 57:
        print("Caractère spécial")
    elif char > 47:
        print("Chiffre")
    else:
        print("Caractère spécial")

findchar(8)
