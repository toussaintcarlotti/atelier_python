import random


def choose_element_list(list_in_which_to_choose: list) -> int:
    """
      fonction qui renvoi un élément tiré au hasard dans la liste passer en paramètre
      @param list_in_which_to_choose: list
      @return: int
    """
    #indice aléatoire généré permettant de tirer un élément aléatoire dans la liste
    return list_in_which_to_choose[random.randint(0, len(list_in_which_to_choose) - 1)]



def test():
    """
    fonction de test
    """
    lst_sorted = [i for i in range(10)]
    print('Liste triée de départ', lst_sorted)
    e1 = choose_element_list(lst_sorted)
    print('A la première exécution', e1, 'a été sélectionné')
    e2 = choose_element_list(lst_sorted)
    print('A la deuxième exécution', e2, 'a été sélectionné')
    assert e1 != e2, "Attention vérifiez votre code, pour deux sélections " \
                     "de suite l'élément sélectionné est le même !"
